package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TaskOrganizerMainTest {

    @Test
    public void test_create_task_manager_and_add_tasks() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager allTasks = new TaskManager();
        allTasks.addTask(new Task(1, 1));
        allTasks.addTask(new Task(2, 2));
        allTasks.addTask(new Task(3, 1));
        allTasks.addTask(new Task(4, 3));
        allTasks.addTask(new Task(5, 1));
        allTasks.addTask(new Task(6, 2));
        allTasks.addTask(new Task(7, 3));
        allTasks.addTask(new Task(8, 1));
        allTasks.addTask(new Task(9, 2));
        allTasks.addTask(new Task(10, 3));
        allTasks.addTask(new Task(11, 10));
        allTasks.addTask(new Task(12, 1));
        allTasks.addTask(new Task(13, 3));
        allTasks.addTask(new Task(14, 2));
        allTasks.addTask(new Task(15, 3));

        assertEquals(allTasks.taskStack.size(), 15);
    }

    @Test
    public void test_create_selected_task_manager_and_add_tasks() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager selectedTasks = new TaskManager();
        selectedTasks.addTask(new Task(3, 1));
        selectedTasks.addTask(new Task(6, 2));
        selectedTasks.addTask(new Task(5, 1));
        selectedTasks.addTask(new Task(2, 2));
        selectedTasks.addTask(new Task(1, 1));

        assertEquals(selectedTasks.taskStack.size(), 5);
    }

    @Test
    public void test_find_highest_id_selected_task_manager() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager selectedTasks = new TaskManager();
        selectedTasks.addTask(new Task(3, 1));
        selectedTasks.addTask(new Task(6, 2));
        selectedTasks.addTask(new Task(5, 1));
        selectedTasks.addTask(new Task(2, 2));
        selectedTasks.addTask(new Task(1, 1));

        selectedTasks.sortTasks();

        int highestSelectedTaskId = selectedTasks.findHighestId();

        assertEquals(highestSelectedTaskId, 6);
    }

    @Test
    public void test_all_tasks_empty() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager allTasks = new TaskManager();

        assertEquals(allTasks.taskStack.size(), 0);
    }

    @Test
    public void test_selected_tasks_empty() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager selectedTasks = new TaskManager();

        assertEquals(selectedTasks.taskStack.size(), 0);
    }

    @Test
    public void test_all_tasks_same_id() {
        TaskOrganizerMain.main(new String[]{});

        TaskManager allTasks = new TaskManager();
        allTasks.addTask(new Task(1, 1));
        allTasks.addTask(new Task(1, 2));
        allTasks.addTask(new Task(1, 3));

        assertEquals(allTasks.taskStack.size(), 3);
    }

}