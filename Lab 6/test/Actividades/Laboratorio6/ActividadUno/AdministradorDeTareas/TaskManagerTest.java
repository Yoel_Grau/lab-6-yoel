package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class TaskManagerTest {

    private TaskManager manager;

    @BeforeEach
    void setUp() {
        manager = new TaskManager();
        manager.addTask(new Task(3, 1));
        manager.addTask(new Task(1, 2));
        manager.addTask(new Task(2, 1));
    }

    @Test
    void testAddTask() {
        Task newTask = new Task(4, 3);
        manager.addTask(newTask);
        assertEquals(4, manager.taskStack.size());
    }

    @Test
    void testSortTasks() {
        manager.sortTasks();
        List<Task> sortedTasks = manager.taskStack;
        assertEquals(2, sortedTasks.get(1).id);
        assertEquals(2, sortedTasks.get(1).id);
        assertEquals(3, sortedTasks.get(2).id);
    }

    @Test
    void testFilterTasks() {
        manager.sortTasks();
        TaskManager filteredManager = manager.filterTasks(2);
        assertEquals(1, filteredManager.taskStack.size());
    }

    @Test
    void testFindHighestId() {
        int highestId = manager.findHighestId();
        assertEquals(3, highestId);
    }

    @Test
    public void test_instantiation_without_errors() {
        TaskManager manager = new TaskManager();
        assertNotNull(manager);
    }

    @Test
    public void test_add_task_without_errors() {
        TaskManager manager = new TaskManager();
        Task task = new Task(1, 1);
        manager.addTask(task);
        assertEquals(1, manager.taskStack.size());
        assertTrue(manager.taskStack.contains(task));
    }

    @Test
    public void test_sort_tasks_without_errors() {
        TaskManager manager = new TaskManager();
        manager.addTask(new Task(3, 1));
        manager.addTask(new Task(1, 2));
        manager.addTask(new Task(2, 1));
        manager.sortTasks();
        assertEquals(1, manager.taskStack.get(0).id);
        assertEquals(2, manager.taskStack.get(1).id);
        assertEquals(3, manager.taskStack.get(2).id);
    }

    @Test
    public void test_find_highest_id_without_errors() {
        TaskManager manager = new TaskManager();
        manager.addTask(new Task(3, 1));
        manager.addTask(new Task(1, 2));
        manager.addTask(new Task(2, 1));
        int highestId = manager.findHighestId();
        assertEquals(3, highestId);
    }


    @Test
    public void test_instantiation_with_duplicate_tasks() {
        TaskManager manager = new TaskManager();
        Task task = new Task(1, 1);
        manager.addTask(task);
        manager.addTask(task);
        assertEquals(2, manager.taskStack.size());
        assertTrue(manager.taskStack.contains(task));
    }

    @Test
    public void test_sort_empty_taskStack_without_errors() {
        TaskManager manager = new TaskManager();
        manager.sortTasks();
        assertEquals(0, manager.taskStack.size());
    }

    @Test
    public void test_filter_empty_taskStack_without_errors() {
        TaskManager manager = new TaskManager();
        TaskManager filteredManager = manager.filterTasks(2);
        assertEquals(0, filteredManager.taskStack.size());
    }


}