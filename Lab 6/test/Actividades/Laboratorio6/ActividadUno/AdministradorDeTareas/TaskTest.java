package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    @Test
    public void test_taskEquality() {
        Task task1 = new Task(1, 1);
        Task task2 = new Task(1, 1);
        assertEquals(task1, task2);
    }

    @Test
    public void test_taskHashCode() {
        Task task1 = new Task(1, 1);
        Task task2 = new Task(1, 1);
        assertEquals(task1.hashCode(), task2.hashCode());
    }

    @Test
    public void test_taskInitialization() {
        Task task = new Task(1, 1);
        assertEquals(1, task.id);
        assertEquals(1, task.priority);
    }

    @Test
    public void test_taskToString() {
        Task task = new Task(1, 1);
        assertEquals("Tarea 1, prioridad 1", task.toString());
    }

    @Test
    public void test_taskInequality() {
        Task task1 = new Task(1, 1);
        Task task2 = new Task(2, 2);
        assertNotEquals(task1, task2);
    }

    @Test
    public void test_taskPriorityZero() {
        Task task = new Task(1, 0);
        assertEquals(0, task.priority);
    }

}
