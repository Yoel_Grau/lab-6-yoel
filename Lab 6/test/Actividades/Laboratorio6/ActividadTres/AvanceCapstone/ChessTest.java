package Actividades.Laboratorio6.ActividadTres.AvanceCapstone;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class chessTest {

    @Test
    void testInsertionSortIntegers() {
        Integer[] original = {5, 3, 8, 4, 2};
        Integer[] sorted = {2, 3, 4, 5, 8};
        InsertionSort<Integer> sorter = new InsertionSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testInsertionSortStrings() {
        String[] original = {"m", "j", "k", "l", "e"};
        String[] sorted = {"e", "j", "k", "l", "m"};
        InsertionSort<String> sorter = new InsertionSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testQuickSortIntegers() {
        Integer[] original = {5, 3, 8, 4, 2};
        Integer[] sorted = {2, 3, 4, 5, 8};
        QuickSort<Integer> sorter = new QuickSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testQuickSortStrings() {
        String[] original = {"m", "j", "k", "l", "e"};
        String[] sorted = {"e", "j", "k", "l", "m"};
        QuickSort<String> sorter = new QuickSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testBubbleSortIntegers() {
        Integer[] original = {5, 3, 8, 4, 2};
        Integer[] sorted = {2, 3, 4, 5, 8};
        BubbleSort<Integer> sorter = new BubbleSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testBubbleSortStrings() {
        String[] original = {"m", "j", "k", "l", "e"};
        String[] sorted = {"e", "j", "k", "l", "m"};
        BubbleSort<String> sorter = new BubbleSort<>();
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    public void testMergeSort() {
        MergeSort<Integer> sorter = new MergeSort<>();
        Integer[] original = { 8, 4, 23, 42, 16, 15 };
        Integer[] sorted = { 4, 8, 15, 16, 23, 42 };
        sorter.sort(original);
        assertArrayEquals(sorted, original);
    }

    @Test
    void testArgumentValidation() {
        String[] args = {"a=q", "t=c", "o=b"};
        assertTrue(Chess.isValidArg(Chess.getArgValue(args, "a"), new String[]{"i", "q"}));
        assertTrue(Chess.isValidArg(Chess.getArgValue(args, "t"), new String[]{"c", "n"}));
        assertTrue(Chess.isValidArg(Chess.getArgValue(args, "o"), new String[]{"b", "w"}));
    }

    @Test
    void testInvalidArgument() {
        String[] args = {"a=x", "t=y", "o=z"};
        assertFalse(Chess.isValidArg(Chess.getArgValue(args, "a"), new String[]{"i", "q"}));
        assertFalse(Chess.isValidArg(Chess.getArgValue(args, "t"), new String[]{"c", "n"}));
        assertFalse(Chess.isValidArg(Chess.getArgValue(args, "o"), new String[]{"b", "w"}));
    }

    @Test
    void testProcessArgumentsValid() {
        String[] args = {"a=q", "t=c", "o=b"};
        Chess.SortingConfiguration config = Chess.processArguments(args);
        assertNotNull(config);
        assertEquals("q", config.sortMethod);
        assertEquals("c", config.type);
        assertEquals("b", config.color);
    }

    @Test
    void testProcessArgumentsInvalid() {
        String[] args = {"a=x", "t=y", "o=z"};
        Chess.SortingConfiguration config = Chess.processArguments(args);
        assertNull(config);
    }

    @Test
    void testPerformSorting() {
        Integer[] values = { 3, 1, 4, 5, 2 };
        Integer[] expected = { 1, 2, 3, 4, 5 };
        SortAlgorithm<Integer> algorithm = new InsertionSort<>();
        Integer[] sortedValues = Chess.performSorting(values, algorithm);
        assertArrayEquals(expected, sortedValues);
    }

}