package Actividades.Laboratorio6.ActividadDos.VotoElectronico;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;


class SistemaVotacionTest {
    private SistemaVotacion sistemaVotacion;

    @BeforeEach
    void setUp() {
        sistemaVotacion = new SistemaVotacion();
    }

    @Test
    void testRealizarVotoYContarCorrectamente() {
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID001"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID002"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato B", "ID003"));
        assertEquals(2, sistemaVotacion.getVotosCandidato("Candidato A"));
        assertEquals(1, sistemaVotacion.getVotosCandidato("Candidato B"));
    }

    @Test
    void testRechazarVotosDuplicados() {
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID004"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID004"));
        assertEquals(1, sistemaVotacion.getVotosCandidato("Candidato A"));
    }

    @Test
    void testRealizarVotoSinDuplicados() {
        sistemaVotacion.realizarVotacion(new Voto("Candidato C", "ID005"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato C", "ID006"));
        assertNotEquals(1, sistemaVotacion.getVotosCandidato("Candidato C"));
    }
}
