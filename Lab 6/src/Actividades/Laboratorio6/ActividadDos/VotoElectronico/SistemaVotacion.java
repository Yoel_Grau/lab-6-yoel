package Actividades.Laboratorio6.ActividadDos.VotoElectronico;

import java.util.LinkedHashMap;
import java.util.Map;

public class SistemaVotacion {
    private LinkedHashMap<String, Integer> ValidezLegal;
    private Map<String, Boolean> votantes;

    public SistemaVotacion() {
        ValidezLegal = new LinkedHashMap<>();
        votantes = new LinkedHashMap<>();
    }

    public void realizarVotacion(Voto voto) {
        if (verificarVoto(voto)) {
            ValidezLegal.put(voto.getCandidato(), ValidezLegal.getOrDefault(voto.getCandidato(), 0) + 1);
            votantes.put(voto.getIdentificador(), true);
        } else {
            System.out.println("Voto duplicado no se contabilizara.");
        }
    }

    public int getVotosCandidato(String candidato) {
        return ValidezLegal.getOrDefault(candidato, 0);
    }

    private boolean verificarVoto(Voto voto) {
        return !votantes.getOrDefault(voto.getIdentificador(), false);
    }

    public void obtenerResultadosVotacion() {
        for (Map.Entry<String, Integer> entrada : ValidezLegal.entrySet()) {
            System.out.println("Candidato " + entrada.getKey() + ", " + entrada.getValue() + " votos");
        }
    }

}

