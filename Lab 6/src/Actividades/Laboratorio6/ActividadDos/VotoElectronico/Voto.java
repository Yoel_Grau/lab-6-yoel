package Actividades.Laboratorio6.ActividadDos.VotoElectronico;

public class Voto {
    private String candidato;
    private String identificador;

    public Voto(String candidato, String identificador) {
        this.candidato = candidato;
        this.identificador = identificador;
    }

    public String getCandidato() {
        return candidato;
    }

    public String getIdentificador() {
        return identificador;
    }
}
