package Actividades.Laboratorio6.ActividadDos.VotoElectronico;

class Main {
    public static void main(String[] args) {
        SistemaVotacion sistemaVotacion = new SistemaVotacion();

        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID123"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID124"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato B", "ID125"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato C", "ID126"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID127"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato B", "ID129"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato B", "ID130"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID131"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID132"));

        sistemaVotacion.realizarVotacion(new Voto("Candidato A", "ID132"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato B", "ID125"));
        sistemaVotacion.realizarVotacion(new Voto("Candidato C", "ID126"));

        sistemaVotacion.obtenerResultadosVotacion();

    }
}
