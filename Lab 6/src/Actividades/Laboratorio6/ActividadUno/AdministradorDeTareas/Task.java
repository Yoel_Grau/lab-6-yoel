package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;

import java.util.Objects;

public class Task {
    int id;
    int priority;

    public Task(int id, int priority) {
        this.id = id;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Tarea " + id + ", prioridad " + priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id && priority == task.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, priority);
    }
}