package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;


public class TaskOrganizerMain {

    public static void main(String[] args) {

        TaskManager allTasks = new TaskManager();
        allTasks.addTask(new Task(1, 1));
        allTasks.addTask(new Task(2, 2));
        allTasks.addTask(new Task(3, 1));
        allTasks.addTask(new Task(4, 3));
        allTasks.addTask(new Task(5, 1));
        allTasks.addTask(new Task(6, 2));
        allTasks.addTask(new Task(7, 3));
        allTasks.addTask(new Task(8, 1));
        allTasks.addTask(new Task(9, 2));
        allTasks.addTask(new Task(10, 3));
        allTasks.addTask(new Task(11, 10));
        allTasks.addTask(new Task(12, 1));
        allTasks.addTask(new Task(13, 3));
        allTasks.addTask(new Task(14, 2));
        allTasks.addTask(new Task(15, 3));

        TaskManager selectedTasks = new TaskManager();
        selectedTasks.addTask(new Task(3, 1));
        selectedTasks.addTask(new Task(6, 2));
        selectedTasks.addTask(new Task(5, 1));
        selectedTasks.addTask(new Task(2, 2));
        selectedTasks.addTask(new Task(1, 1));

        selectedTasks.sortTasks();

        System.out.println("Tareas Organizadas:");
        selectedTasks.printTasks();

        int highestSelectedTaskId = selectedTasks.findHighestId();

        TaskManager auxiliaryList = allTasks.filterTasks(highestSelectedTaskId);

        System.out.println("\nLista Auxiliar:");
        auxiliaryList.printTasks();
    }
}
