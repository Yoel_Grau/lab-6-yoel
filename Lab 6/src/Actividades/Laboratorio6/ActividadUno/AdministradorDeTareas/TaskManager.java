package Actividades.Laboratorio6.ActividadUno.AdministradorDeTareas;

import java.util.*;

public class TaskManager {
    public Stack<Task> taskStack;

    public TaskManager() {
        this.taskStack = new Stack<>();
    }

    public void addTask(Task task) {
        taskStack.push(task);
    }

    public void sortTasks() {
        List<Task> list = new ArrayList<>(taskStack);
        Collections.sort(list, new Comparator<Task>() {
            @Override
            public int compare(Task t1, Task t2) {
                if (t1.priority == t2.priority) {
                    return t1.priority == 1 ? Integer.compare(t1.id, t2.id) : Integer.compare(t2.id, t1.id);
                }
                return Integer.compare(t2.priority, t1.priority);
            }
        });
        taskStack.clear();
        taskStack.addAll(list);
    }

    public TaskManager filterTasks(int thresholdId) {
        TaskManager filteredManager = new TaskManager();
        for (Task task : taskStack) {
            if (task.id > thresholdId) {
                filteredManager.addTask(task);
            }
        }
        return filteredManager;
    }

    public int findHighestId() {
        return taskStack.stream().max(Comparator.comparingInt(task -> task.id)).orElse(new Task(0, 0)).id;
    }

    public void printTasks() {
        taskStack.forEach(System.out::println);
    }
}
