package Actividades.Laboratorio6.ActividadTres.AvanceCapstone;

public abstract class SortAlgorithm<T extends Comparable<T>> {
    public abstract void sort(T[] array);
    protected void swap(T[] array, int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

