package Actividades.Laboratorio6.ActividadTres.AvanceCapstone;


import java.util.Arrays;
import java.util.Random;
public class Chess {

    public static void main(String[] args) {

        String[] stringValues = {"m", "j", "k", "l", "e", "n", "c", "d", "b", "g", "h", "i", "f", "o", "p", "a"};
        Integer[] intValues = {5, 8, 15, 16, 1, 2, 9, 10, 11, 6, 7, 13, 14, 3, 4, 12};

        String sortMethod = getArgValue(args, "a");
        String type = getArgValue(args, "t");
        String color = getArgValue(args, "o");

        SortAlgorithm<?>[] algorithms = {
                new InsertionSort<>(),
                new QuickSort<>(),
                new BubbleSort<>(),
                new MergeSort<>()
        };

        Random random = new Random();
        SortAlgorithm<?> selectedAlgorithm = algorithms[random.nextInt(algorithms.length)];

        if (!isValidArg(sortMethod, new String[]{"i", "q"}) || !isValidArg(type, new String[]{"c", "n"}) || !isValidArg(color, new String[]{"b", "w"})) {
            System.out.println("Ordenamiento: Invalido");
            System.out.println("Tipo: Invalido");
            System.out.println("Color: Invalido");
            System.out.println("Valores: []");
            System.out.println("Valores Invalidos");
            //return;
        }
        else if (type.equals("c")) {
            System.out.println("Ordenamiento: [" + getSortName(sortMethod) + "]");
            System.out.println("Tipo: [Caracter]");
            System.out.println("Color: [" + getColorName(color) + "]");
            System.out.println("Valores: " + Arrays.toString(stringValues));
            ((SortAlgorithm<String>) selectedAlgorithm).sort(stringValues);
            System.out.println("Ordenamiento: " + Arrays.toString(stringValues));
        } else {
            System.out.println("Ordenamiento: [" + getSortName(sortMethod) + "]");
            System.out.println("Tipo: [Numerico]");
            System.out.println("Color: [" + getColorName(color) + "]");
            System.out.println("Valores: " + Arrays.toString(intValues));
            ((SortAlgorithm<Integer>) selectedAlgorithm).sort(intValues);
            System.out.println("Ordenamiento: " + Arrays.toString(intValues));
        }
    }

    public static SortingConfiguration processArguments(String[] args) {
        String sortMethod = getArgValue(args, "a");
        String type = getArgValue(args, "t");
        String color = getArgValue(args, "o");

        if (!isValidArg(sortMethod, new String[]{"i", "q"}) ||
                !isValidArg(type, new String[]{"c", "n"}) ||
                !isValidArg(color, new String[]{"b", "w"})) {
            return null;
        }

        return new SortingConfiguration(sortMethod, type, color);
    }

    public static <T extends Comparable<T>> T[] performSorting(T[] values, SortAlgorithm<T> algorithm) {
        algorithm.sort(values);
        return values;
    }

    public static class SortingConfiguration {
        String sortMethod;
        String type;
        String color;

        public SortingConfiguration(String sortMethod, String type, String color) {
            this.sortMethod = sortMethod;
            this.type = type;
            this.color = color;
        }

    }

    public static String getArgValue(String[] args, String key) {
        for (String arg : args) {
            if (arg.startsWith(key + "=")) {
                return arg.substring(arg.indexOf('=') + 1);
            }
        }
        return null;
    }

    public static boolean isValidArg(String arg, String[] validArgs) {
        for (String validArg : validArgs) {
            if (validArg.equals(arg)) {
                return true;
            }
        }
        return false;
    }

    private static String getSortName(String sortMethod) {
        switch (sortMethod) {
            case "i":
                return "Insertion sort";
            case "q":
                return "Quick sort";
            default:
                return "Invalido";
        }
    }

    private static String getColorName(String color) {
        switch (color) {
            case "b":
                return "Negras";
            case "w":
                return "Blancas";
            default:
                return "Invalido";
        }
    }
}




