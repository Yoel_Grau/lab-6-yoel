package Actividades.Laboratorio6.ActividadTres.AvanceCapstone;

public class BubbleSort<T extends Comparable<T>> extends SortAlgorithm<T> {
    @Override
    public void sort(T[] array) {
        boolean swapped;
        do {
            swapped = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i - 1].compareTo(array[i]) > 0) {
                    swap(array, i - 1, i);
                    swapped = true;
                }
            }
        } while (swapped);
    }
}
